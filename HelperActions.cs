﻿using OpenQA.Selenium;
using System.Drawing;
using System.Drawing.Imaging;
using TechTalk.SpecFlow;

namespace GettingStartedWithAutomation
{
    class HelperActions
    {

        public AppSizePositionInfo GetAppSizeAndPositionInfo(IWebDriver driver)
        {
            AppSizePositionInfo info = new AppSizePositionInfo();
            info.appHeight = driver.Manage().Window.Size.Height;
            info.appWidth = driver.Manage().Window.Size.Width;
            info.appLeftPosition = driver.Manage().Window.Position.X;
            info.appTopPosition = driver.Manage().Window.Position.Y;

            return info;
        }
        public void TakeScreenShot(string directory, IWebDriver driver)
        {
            CreateDirectory(directory);
            string nextScreenShot = $"{directory}\\screenshot-{(CountCurrentFiles(directory) + 1).ToString()}.Jpeg";
            TakeScreenShot(nextScreenShot, GetAppSizeAndPositionInfo(driver));
        }

        private void CreateDirectory(string directory)
        {
            if(!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
        }

        private int CountCurrentFiles(string directory)
        {
            return System.IO.Directory.GetFiles(directory).Length;
        }

        private void TakeScreenShot(string imageName, AppSizePositionInfo info)
        {
            System.Threading.Thread.Sleep(500);

            Size sapSize = new Size(info.appWidth, info.appHeight);

            using (Bitmap bitmap = new Bitmap(info.appWidth, info.appHeight))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(info.appLeftPosition, info.appTopPosition), Point.Empty, sapSize);
                }


                bitmap.Save(imageName, ImageFormat.Jpeg);
            }
        }
    }

    public class AppSizePositionInfo
    {
        public int appWidth { get; set; }
        public int appHeight { get; set; }
        public int appLeftPosition { get; set; }
        public int appTopPosition { get; set; }
    }
}
