﻿Feature: ChromeTests

Scenario: Open Google and Search
	Given https://www.google.nl is opened
	And the cookies are accepted
	When I search for Qquest
	Then I expect the window title to be Qquest - Google zoeken

Scenario: Open Zoekmachine and Search
	Given https://www.google.nl is opened
	And the cookies are accepted
	When I search for Qquest
	Then I expect the window title to be Qquest - Google zoeken

Scenario: Open Tommy UAT
	Given https://nl.b2ceuup.tommy.com is opened
	Then I wait for the cookie message
