﻿using TechTalk.SpecFlow;
using OpenQA.Selenium;
using NUnit.Framework;
using pvh_sap_connector;
using System;
using OpenQA.Selenium.Support.UI;

namespace GettingStartedWithAutomation
{
    [Binding]
    class Bindings
    {
        private IWebDriver driver;
        private SAPConnector sap;
        public Bindings(ScenarioContext testContext)
        {
            driver = testContext.Get<IWebDriver>("driver");
            sap = testContext.Get<SAPConnector>("sapconnector");
        }
        [Given(@"(.*) is opened")]
        public void GivenHttpsWww_Google_NlIsOpened(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        [Given(@"the cookies are accepted")]
        public void GivenTheCookiesAreAccepted()
        {
            By acceptButton = By.XPath("//button[contains(string(),'Ik ga akkoord')]");
            driver.FindElement(acceptButton).Click();
        }

        [When(@"I search for (.*)")]
        public void WhenISearchForQquest(string searchQuery)
        {
            By searchInput = By.XPath("//input[@class='gLFyf gsfi']");
            driver.FindElement(searchInput).SendKeys(searchQuery);
            driver.FindElement(searchInput).Submit();
        }

        [Then(@"I expect the window title to be (.*)")]
        public void ThenIExpectTheWindowTitleToBeQquest_GoogleZoeken(string expectedTabTitle)
        {
            string tabTitle = driver.Title;
            Assert.IsTrue(tabTitle.Equals(expectedTabTitle), $"Expected {expectedTabTitle} but found {tabTitle}");
        }

        [Given(@"I have SAP open")]
        public void GivenIHaveSAPOpen()
        {
            sap.OpenSap("SAP FMS Quality Assurance");
            sap.Login("300", "johsap", "PvhTesting6!", "EN");
        }

        [When(@"I goto (.*) and search for po-number (.*)")]
        public void WhenIGotoVAAndSearchForPo_Number(string p0, string p1)
        {   
            sap.StartTransaction(p0);
            sap.SetTextField("Purchase Order No." , 0, p1);
            sap.ClickButton("Search", 0);
            sap.SendKeys("ENTER");
        }

        [Then(@"I fetch the SAPOrderNumber")]
        public void ThenIFetchTheSAPOrderNumber()
        {
            sap.SendKeys("BACK");
            string sapOrderNumber = sap.GetTextField("VBAK-VBELN", 0);
            Console.WriteLine(sapOrderNumber);
            Assert.IsTrue(sapOrderNumber.Equals("162771509"), $"Expected 162771509 but found : {sapOrderNumber}");
        }

        [Then(@"I wait for the cookie message")]
        public void ThenIWaitForTheCookieMessage()
        {
            By cookieNotice = By.XPath("//div[@class='cookie-notice']");
            bool visible = false;
            try
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(driver => driver.FindElement(cookieNotice).Displayed);
                visible = true;
            }
            catch
            {

                visible = false;
            }

            Assert.IsTrue(visible);
        }


    }
}
