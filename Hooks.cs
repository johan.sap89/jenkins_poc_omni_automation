﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using pvh_sap_connector;
using System.Diagnostics;
using System;

namespace GettingStartedWithAutomation
{
    [Binding]
    class Hooks
    {
        [BeforeScenario]
        public void BeforeScenario(ScenarioContext testContext)
        {
            testContext.Add("driver", new ChromeDriver());
            testContext.Add("sapconnector", new SAPConnector());
            testContext.Add("screenshotFolder", System.IO.Directory.GetCurrentDirectory() + "\\screenshots");
        }

        [AfterScenario]
        public void AfterScenario(ScenarioContext testContext)
        {
            try
            {
                new HelperActions().TakeScreenShot(testContext.Get<string>("screenshotFolder"), testContext.Get<IWebDriver>("driver"));
                testContext.Get<IWebDriver>("driver").Close();
                testContext.Get<IWebDriver>("driver").Quit();

                if (!testContext.Get<SAPConnector>("sapconnector").ReturnConnectionNumber().Equals(0))
                {
                    testContext.Get<SAPConnector>("sapconnector").CloseSAP();
                }
            }
            catch
            {
                Console.WriteLine("Closing the driver failed");
            }
        }

    }
}
